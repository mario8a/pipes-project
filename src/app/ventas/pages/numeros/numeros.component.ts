import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-numeros',
  templateUrl: './numeros.component.html',
  styles: [
  ]
})
export class NumerosComponent implements OnInit {

  ventasNetas:number = 2855454.555;
  porcentaje: number = 0.45;

  constructor() { }

  ngOnInit(): void {
  }

}
