import { Component } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent {

  nombreLower: string = 'mario';
  nombreUpper: string = 'MARIO';
  nombreCOmpleto: string = 'mARio oChoA';

  fecha: Date = new Date() // El dia de hoy

}
