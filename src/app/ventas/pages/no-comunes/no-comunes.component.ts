import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styles: [
  ]
})
export class NoComunesComponent implements OnInit {

  // i18nSelect
  nombre: string = 'Mario';
  genero:string = 'masculino';

  invitacionMapa = {
    'masculino': 'invitarlo',
    'femenino': 'invitarla'
  }

  //i18nPlural
  clientes: string[] = ['Maria', 'Pedro', 'Estela', 'Vanessa', 'Wendy', 'Pepe']
  clientesMapa = {
    '=0': 'No tenemos ningun cliente esperando',
    '=1': 'Tenemos 1 cliente esperando',
    '=2': 'Tenemos 2 clientes esperando',
    'other': 'Tenemos # clientes esperando'
  }

  constructor() { }

  ngOnInit(): void {
  }

  cambiarCliente() {
    // Cambiar de Mario a Vanessa 
    // Cambia el genero de masculino a masculino
    this.nombre = 'Vanessa';
    this.genero = 'femenino'
  }

  borrarCliente() {
    // La lista debe actualizarse

    this.clientes.pop();
  }

  // ValuePipe

  persona= {
    nombre: 'Mario',
    edad: 65,
    direccion: 'Mexico DF'
  }

  // JSON pipe
  heroe = [
    {
      nombre: 'Superman',
      vuela: true
    },
    {
      nombre: 'Robin',
      vuela: false
    },
    {
      nombre: 'Gatube',
      vuela: false
    }
  ]

  // ASync pipe

  miObservable = interval(1000);

  valorPromesa = new Promise((res, rej) => {
    setTimeout(() => {
      res('Fin de la promesa')
    }, 3500)
  })

}
